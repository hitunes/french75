const makeObjectsOfArrayUnique = (arr, property) => {
  // const allArticles = [...state.all, ...action.payload];
  const result = [];
  const map = new Map();

  for (const element of arr) {
    if (!map.has(element[property])) {
      map.set(element[property], true);
      result.push(element);
    }
  }

  return result;
};

export { makeObjectsOfArrayUnique };
