/**
 * Formats prices into readable formats
 * @param {Number || String} amount The amount you want to format
 */
const formatAmount = amount => {
  return amount
    .toString()
    .replace(",", "")
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

export { formatAmount };
