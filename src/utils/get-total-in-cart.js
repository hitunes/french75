const getTotalInCart = cartItems => {
  return cartItems.reduce((acc, cartItem) => {
    return acc + cartItem.priceTotal;
  },0);
};

export { getTotalInCart };
