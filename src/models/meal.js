/**
 *
 * @param {Object} mealData An object containing the all data related to a meal
 */
class Meal {
  constructor(mealData) {
    const { id, image, name, extras, price, quantity } = mealData;

    this.id = id;
    this.image = this.transformImage(image);
    this.name = name;
    this.extras = this.transformExtras(extras);
    this.price = price;
    this.quantity = quantity;
  }

  transformImage(image) {
    if (image.fields) {
      return `https:${image.fields.file.url}`;
    } else {
      return image;
    }
  }

  transformExtras(extras) {
    if (!extras || extras.length === 0) return [];

    if (extras[0].id) {
      return extras.map(extra => new ExtraId(extra));
    } else {
      return extras
        .filter(extra => 'fields' in extra)
        .map(extra => new Extra(extra));
    }
  }
}

class Extra {
  constructor(extraData) {
    const id = extraData.sys.id;
    const { name, price } = extraData.fields;

    this.id = id;
    this.name = name;
    this.price = price;
  }
}
class ExtraId {
  constructor(extraData) {
    const { id, name, price } = extraData;

    this.id = id;
    this.name = name;
    this.price = price;
  }
}
export default Meal;
