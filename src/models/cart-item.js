class CartItem {
  constructor(item) {
    const { id, name, image, extras, price, quantity,selectedExtras =[] } = item;

    this.id = id;
    this.name = name;
    this.image = image;
    this.extras = extras;
    this.price = price;
    this.quantity = quantity;
    this.priceTotal = this.getPriceTotal();
    this.selectedExtras = selectedExtras;
  }

  getPriceTotal() {
    return this.price * this.quantity;
  }
}

export default CartItem;
