import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import promiseMiddleware from 'redux-promise-middleware';

import rootReducer from "./reducers";

const loggerMiddleware = createLogger();
const middleware = [thunk, promiseMiddleware, loggerMiddleware];

const store = createStore(
  rootReducer,
  applyMiddleware(...middleware)
);

export default store;
