import {
  ADD_MEAL_TO_CART,
  REMOVE_MEAL_FROM_CART,
  ADD_EXTRAS_TO_MEAL,
  REMOVE_EXTRAS_FROM_MEAL,
} from '../actions/cart/cart-types';
import CartItem from '../../models/cart-item';
import { getTotalInCart } from '../../utils/get-total-in-cart';

const initialState = {
  items: [],
  total: 0,
  extraTotal: 0,
  isOrderComplete: false,
};

const cartReducer = (state = initialState, action) => {
  const { type, payload } = action;

  let items;
  let index;
  switch (type) {
    case ADD_MEAL_TO_CART:
      // Get the index if meal exists in cart
      items = state.items;
      index = state.items.findIndex(item => item.id === payload.id);

      if (index !== -1) {
        // Update if existent
        let cartItem = items[index];
        cartItem = new CartItem({
          ...cartItem,
          quantity: cartItem.quantity + 1,
        });
        items[index] = cartItem;
        let overallTotal = getTotalInCart(items);
        return { ...state, items, total: overallTotal };
      } else {
        // Add if non-existent
        const cartItem = new CartItem({ ...payload, quantity: 1 });
        return { ...state, items: [...items, cartItem] };
      }

    case REMOVE_MEAL_FROM_CART:
      items = state.items;
      index = state.items.findIndex(item => item.id === payload.id);

      if (index !== -1 && items[index].quantity > 0) {
        let cartItem = items[index];
        cartItem = new CartItem({
          ...cartItem,
          quantity: cartItem.quantity - 1,
        });
        items[index] = cartItem;
        let overallTotal = getTotalInCart(items);
        return { ...state, items, total: overallTotal };
      } else {
        items = items.splice(index, 1);
        return { ...state, items };
      }

    case ADD_EXTRAS_TO_MEAL:
      items = [...state.items];
      index = items.findIndex(item => item.id === payload.parentId);

      if (index !== -1) {
        let cartItem = items[index];
        cartItem = {
          ...cartItem,
          selectedExtras: [...cartItem.selectedExtras, payload.extra],
        };
        items[index] = cartItem;
        let extraTotal = calcTotalExtras(items);
        return { ...state, items, extraTotal };
      }
      break;

    case REMOVE_EXTRAS_FROM_MEAL:
      items = state.items;
      index = state.items.findIndex(item => item.id === payload.parentId);
      if (index !== -1) {
        let cartItem = items[index];
        let selectedExtras = cartItem.selectedExtras.filter(extra => {
          return extra.id !== payload.extra.id;
        });
        cartItem = { ...cartItem, selectedExtras };
        items[index] = cartItem;
        let extraTotal = calcTotalExtras(items);
        return { ...state, items, extraTotal };
      }
      break;
    case 'COMPLETE_ORDER_FULFILLED':
      return { ...state, isOrderComplete: true, items: [] };
    case 'COMPLETE_ORDER_REJECTED':
      return { ...state, isOrderComplete: true };
    default:
      return state;
  }
};

export { cartReducer };
function calcTotalExtras(items) {
  return items
    .map(item => {
      return item.selectedExtras.reduce((acc, cartItem) => {
        return acc + cartItem.price;
      }, 0);
    })
    .reduce((acc, next) => acc + next, 0);
}
