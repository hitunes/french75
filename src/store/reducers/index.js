/*jshint esversion: 6 */
import { combineReducers } from "redux";
import { mealsReducer } from "./meals-reducer";
import { cartReducer } from "./cart-reducer";

export default combineReducers({
  cart: cartReducer,
  meals: mealsReducer
});
