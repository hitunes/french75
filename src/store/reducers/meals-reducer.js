import * as types from '../actions/meals/meals-types';
import Meal from '../../models/meal';

const initialState = {
  meals: [],
  filter: 'ALL',
  initialMeals: [],
};

const mealsReducer = (state = initialState, action) => {
  const { type, payload } = action;

  let items;
  let index;
  switch (type) {
    case types.ADD_MEALS:
      return {
        ...state,
        meals: [...state.meals, ...payload],
        initialMeals: [...state.meals, ...payload],
      };
    case types.RESET_MEALS:
      return { ...state, meals: [...state.initialMeals] };

    case types.INCREASE_MEAL_QTY:
      items = [...state.meals];
      index = items.findIndex(item => item.id === payload.id);
      if (index !== -1) {
        let mealItem = items[index];
        mealItem = new Meal({
          ...payload,
          extras:[...mealItem.extras],
          quantity: mealItem.quantity + 1,
        });
        items[index] = mealItem;
        return { ...state, meals:items };
      } else {
        const mealItem = new Meal({ ...payload, quantity: 1 });
        return { ...state, meals: [...items, mealItem] };
      }

    case types.DECREASE_MEAL_QTY:
      items = [...state.meals];
      index = items.findIndex(item => item.id === payload.id);
      if (index !== -1 && items[index].quantity > 0) {
        let mealItem = items[index];
        mealItem = new Meal({
          ...mealItem,
          quantity: mealItem.quantity - 1,
        });
        items[index] = mealItem;
      }
      return { ...state, meals: items };
      case 'COMPLETE_ORDER_FULFILLED':
          return { ...state, meals: state.initialMeals};
    default:
      return state;
  }
  }


export { mealsReducer };
