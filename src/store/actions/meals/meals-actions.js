import * as types from "./meals-types";
const actionCreator = type=>{
  return payload=>({
    type: type,
    payload
  })
}

export const addMeals = actionCreator(types.ADD_MEALS);
export const getMeals = actionCreator(types.GET_MEALS);
export const resetMeals = actionCreator(types.RESET_MEALS);
export const increaseMealQty = actionCreator(types.INCREASE_MEAL_QTY);
export const decreaseMealQty = actionCreator(types.DECREASE_MEAL_QTY);
