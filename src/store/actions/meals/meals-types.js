export const ADD_MEALS = "ADD_MEALS";
export const GET_MEALS = "GET_MEALS";
export const RESET_MEALS = "RESET_MEALS";
export const INCREASE_MEAL_QTY = "INCREASE_MEAL_QTY";
export const DECREASE_MEAL_QTY = "DECREASE_MEAL_QTY";
