import * as types from './cart-types';
import axios from 'axios';
import { message } from 'antd';
const actionCreator = type => {
  return payload => ({
    type: type,
    payload,
  });
};
export const completeOrder = payload => {
  let cart = {
    user: payload.user,
    cart: JSON.stringify(payload.cart),
    TOTAL: payload.TOTAL,
  };
  return dispatch => {
    dispatch({
      type: types.COMPLETE_ORDER,
      payload: new Promise(resolve => {
        axios
          .post(`https://usebasin.com/f/5dc4962374a2.json`, cart)
          .then(response =>
            resolve(response.data, message.success('Order Complete')),
          )
          .catch(error => message.error(`${error.response}`, 4));
      }),
    });
  };
};

export const addMealToCart = actionCreator(types.ADD_MEAL_TO_CART);
export const addExtrasToMeal = actionCreator(types.ADD_EXTRAS_TO_MEAL);
export const removeMealFromCart = actionCreator(types.REMOVE_MEAL_FROM_CART);
export const removeExtrasFromMeal = actionCreator(
  types.REMOVE_EXTRAS_FROM_MEAL,
);
