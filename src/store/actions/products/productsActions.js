import types from "./productTypes";
const handleSearchOnclick = actionCreator(types.HANDLE_SEARCH);

function actionCreator(type) {
  return payload => ({
    type: type,
    payload
  });
}

export default {
  handleSearchOnclick
};
