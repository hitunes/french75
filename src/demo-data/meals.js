import Meal from "../models/meal";

// DUMMY CONTENT
const meal = new Meal({
  image: "../assets/sample-food.png",
  alt: "Sample alt",
  name: "Ofada Rice",
  extras: ["plantain", "moi-moi", "salad", "beef", "salad", "beef"],
  unitPrice: 8000
});

const meals = Array(16).fill(meal);

export { meals };
