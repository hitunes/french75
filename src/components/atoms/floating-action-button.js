import React from "react";
import { Button, Icon } from "antd";

const FloatingActionButton = props => {
  const { icon, handleClick } = props;

  return (
    <Button
      className="fab button--primary"
      size="large"
      shape="circle"
      onClick={handleClick}
    >
      <Icon type={icon} />
    </Button>
  );
};

export default FloatingActionButton;
