/**
 * * Single entrypoint to access all atoms.
 * * All atoms will be imported into here and exported as named modules.
 */

import InputField from "./input";
import Pill from "./pill";
import Button from "./button";
import FloatingActionButton from "./floating-action-button";
import Spinner from "./spinner";

export { Button, FloatingActionButton, InputField, Pill, Spinner };
