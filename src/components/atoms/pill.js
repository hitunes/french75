import React, { useState } from "react";
import { Tag } from "antd";
import classNames from "classnames";

const { CheckableTag } = Tag;

const Pill = props => {
  const { extra, handleClick } = props;
  const [checked, setChecked] = useState(false);

  const classes = classNames({
    pill: true,
    "pill--selected": checked
  });

  const handlePillClick = () => {
    setChecked(!checked);
    handleClick(extra, checked);
  };

  return (
    <CheckableTag
      className={classes}
      checked={checked}
      onChange={handlePillClick}
    >
      {extra.name}
    </CheckableTag>
  );
};

export default Pill;
