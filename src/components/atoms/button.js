import React from "react";
import classNames from "classnames";

import { Spinner } from ".";

const Button = props => {
  const { children, icon, kind, loading, handleClick } = props;

  const classes = classNames({
    button: true,
    "button--primary": kind === "primary",
    "button--secondary": kind === "secondary"
  });

  const handleButtonClick = e => {
    handleClick();
  };

  return (
    <button className={classes} onClick={handleButtonClick}>
      {!loading ? <div>{children}</div> : <Spinner size={16} />}
      <div>{icon}</div>
    </button>
  );
};

export default Button;
