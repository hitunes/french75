import React from 'react';
import { FiLoader } from "react-icons/fi";

const Spinner = (props) => {
  const {size} = props;

  return (
    <span className="spinner">
      <FiLoader size={size}/>
    </span>
  )
}

export default Spinner
