import React from "react";
import classNames from "classnames";

const InputField = props => {
  const { name, placeholder, type, handleChange, required } = props;

  const classes = classNames({
    "input-field": true
  });

  const specialId = `input-${parseInt(Math.random() * 100)}`;

  return (
    <label className={classes} htmlFor={specialId} onChange={handleChange}>
      <input
        className="input-field__input"
        type={type || "text"}
        id={specialId}
        name={name}
        required={required || true}
        placeholder="&nbsp;"
      />
      <span className="input-field__label">{placeholder}</span>
    </label>
  );
};

export default InputField;
