import React from "react";
import { connect } from "react-redux";
import { Button, Drawer, Empty } from "antd";
import CartItem from "../molecules/cart-item";
import { formatAmount } from "../../utils/format-amount";

const ShoppingCart = props => {
  const {
    items,
    total,
    extraTotal,
    placement,
    visible,
    updateVisible,

    handleOrderFormVisible
  } = props;
console.log(items)
  return (
    <Drawer
      className="shopping-cart"
      visible={visible}
      title="Your Cart"
      placement={placement}
      onClose={updateVisible}
      width={placement === "right" ? "30%" : "100%"}
    >
      {items.length ? (
        items.map(item => <CartItem key={item.id} item={item} />)
      ) : (
        <Empty
          image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
          imageStyle={{
            height: 60
          }}
          description={<span>No meals here yet!</span>}
        ></Empty>
      )}

      <div className="grid-x shopping-cart__footer">
        <div className="cell small-12 grid-x align-justify">
          <h4 className="cell small-6 h5">Total:</h4>
          <h4 className="cell small-6 h5 color-black text-right">
            N{formatAmount(total + extraTotal)}
          </h4>
        </div>
        <Button
          block
          onClick={handleOrderFormVisible}
          className="button--primary"
          disabled={items.length ? false : true}
        >
          Complete Order
        </Button>
      </div>
    </Drawer>
  );
};

const mapStateToProps = state => {
  return {
    items: state.cart.items,
    total: state.cart.total,
    extraTotal: state.cart.extraTotal
  };
};


export default connect(
  mapStateToProps,
  null
)(ShoppingCart);
