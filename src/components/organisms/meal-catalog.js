import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Spin } from "antd";
import { motion } from "framer-motion";

import Meal from "../../models/meal";
import { MealCard } from "../molecules/";
import {
  addMealToCart,
  removeMealFromCart,
  addExtrasToMeal,
  removeExtrasFromMeal
} from "../../store/actions/cart/cart-actions";
import {
  addMeals,
  increaseMealQty,
  decreaseMealQty
} from "../../store/actions/meals/meals-actions";
import { contentfulClient } from "../../services/contentful-client";

const MealCatalog = props => {
  const {
    meals,
    addMeals,
    addMealToCart,
    addExtrasToMeal,
    increaseMealQty,
    decreaseMealQty,
    removeMealFromCart,
    removeExtrasFromMeal
  } = props;

  const variants = {
    initial: {
      transition: {
        staggerChildren: 0.1
      }
    },
    animate: { transition: { ease: "easeInOut", staggerChildren: 0.3 } }
  };

  const mealVariants = {
    initial: {
      y: "100%"
    },
    animate: {
      y: 0
    }
  };

  useEffect(() => {
    // Get meals from contentful
    const fetchMeals = async () => {
      const data = await contentfulClient.getEntries();
      const meals = data.items.filter(
        item => item.sys.contentType.sys.id === "meals"
      );
      const refinedMeals = meals.map(
        meal => new Meal({ ...meal.fields, id: meal.sys.id, quantity:0 })
      );

      addMeals(refinedMeals);
    };
    fetchMeals();
  }, [addMeals]);

  return (
    <div className="grid-x section--lg">
      <h3 className="h3">Choose your meal</h3>
      <motion.div
        className="grid-x grid-margin-x grid-padding-y medium-up-3 section--sm meal-catalog"
        animate={meals.length ? "animate" : "initial"}
        variants={variants}
      >
        {meals.length === 0 ? (
          <div className="grid-x align-center align-middle meal-catalog__spinner">
            <Spin className="spinner" size="large" />
          </div>
        ) : (
          meals.map(meal => (
            <motion.div className="cell" key={meal.id} variants={mealVariants}>
              <MealCard
                meal={meal}
                addMealToCart={addMealToCart}
                  addExtrasToMeal={addExtrasToMeal}
                  decreaseMealQty={decreaseMealQty}
                  increaseMealQty={increaseMealQty}
                  removeMealFromCart={removeMealFromCart}
                  removeExtrasFromMeal={removeExtrasFromMeal}
              />
            </motion.div>
          ))
        )}
      </motion.div>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    meals: state.meals.meals
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addMeals: payload => dispatch(addMeals(payload)),
    addMealToCart: meal => dispatch(addMealToCart(meal)),
    removeMealFromCart: meal => dispatch(removeMealFromCart(meal)),
    addExtrasToMeal: payload => dispatch(addExtrasToMeal(payload)),
    increaseMealQty: payload => dispatch(increaseMealQty(payload)),
    decreaseMealQty: payload => dispatch(decreaseMealQty(payload)),
    removeExtrasFromMeal: payload => dispatch(removeExtrasFromMeal(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MealCatalog);
