import React from 'react';
import { connect } from 'react-redux';
import { Button, Drawer, Input } from 'antd';
import { InputField } from '../atoms';
import { completeOrder } from '../../store/actions/cart/cart-actions';
import { formatAmount } from '../../utils/format-amount';
const { TextArea } = Input;

class OrderForm extends React.Component {
  state = {
    name: '',
    phone: '',
    address: '',
    message: '',
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    let cart = this.props.cart.map(cart => {
      let extra = cart.selectedExtras.map(extra => ({name:extra.name, price: extra.price}));
      return {
        item: cart.name,
        quantity: cart.quantity,
        unitPrice: cart.price,
        extras: extra,
        unitTotal: `N ${formatAmount(cart.price * cart.quantity)}`,
      };
    });
    this.props.completeOrder({
      user: {
        name: this.state.name,
        phone: this.state.phone,
        address: this.state.address,
        message: this.state.message,
      },
      cart,
      TOTAL: this.props.total
    });
  };
  render() {
    const {
      placement,
      orderFormVisible,
      handleOrderFormVisible,
      cart,
      isOrderComplete,
    } = this.props;
    console.log(cart);
    return (
      <Drawer
        className="order-form"
        visible={orderFormVisible}
        title="Complete Order"
        placement="right"
        onClose={handleOrderFormVisible}
        width={placement === 'right' ? '30%' : '100%'}
      >
        <div className="grid-container">
          <div className="grid-x align-center">
            <div className="cell large-12">
              <form className="form">
                <div className="form-group">
                  <InputField
                    type="text"
                    name="name"
                    placeholder="Name"
                    handleChange={e => this.handleChange(e)}
                  />
                </div>
                <div className="form-group">
                  <InputField
                    type="number"
                    name="phone"
                    placeholder="Phone Number"
                    handleChange={e => this.handleChange(e)}
                  />
                </div>
                <div className="form-group">
                  <InputField
                    type="text"
                    name="address"
                    placeholder="Address"
                    handleChange={e => this.handleChange(e)}
                  />
                </div>
                <div className="form-group">
                  <TextArea
                    autoSize={false}
                    name="message"
                    placement="Special Instructions"
                    onChange={e => this.handleChange(e)}
                  ></TextArea>
                </div>
                <Button
                  onClick={this.handleSubmit}
                  className="button--primary"
                  block
                  type="submit"
                >
                  Complete Order
                </Button>
              </form>
            </div>
          </div>
        </div>
      </Drawer>
    );
  }
}
const mapStateToProps = state => {
  return {
    cart: state.cart.items,
    total: state.cart.total,
    isOrderComplete: state.cart.isOrderComplete,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    completeOrder: cart => dispatch(completeOrder(cart)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OrderForm);
