import React from "react";
import { formatAmount } from "../../utils/format-amount";

const CartItem = props => {
  const { image, name, selectedExtras, quantity, priceTotal } = props.item;
  return (
    <div className="grid-x cart-item">
      <div className="cell small-2 cart-item__image">
        <img src={image} alt="name" />
      </div>
      <div className="cell small-10 grid-x align-justify cart-item__content">
        <div className="cell small-8 grid-y cart-item__content-details">
          <h4 className="h6 color-black font-bold">{name}</h4>
          <p className="color-gray-1">
            {selectedExtras.map((extra, i) => {
              if (selectedExtras.length === i + 1) {
                return <span key={i}>{extra.name}({formatAmount(extra.price)})</span>;
              } else {
                return <span key={i}>{extra.name}({formatAmount(extra.price)}), </span>;
              }
            })}
          </p>
        </div>
        <div className="cell small-3 cart-item__content-price">
          <h4 className="h6 text-right font-bold">
            {formatAmount(priceTotal)}
          </h4>
          <p className="text-right color-gray-1">Amt: {quantity}</p>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
