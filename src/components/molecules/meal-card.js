import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';

import { Pill } from "../atoms";
import { formatAmount } from "../../utils/format-amount";

const MealCard = props => {
  const {
    meal,
    addMealToCart,
    addExtrasToMeal,
    increaseMealQty,
    decreaseMealQty,
    removeMealFromCart,
    removeExtrasFromMeal,
  } = props;
  const { image, alt, name, extras, price, quantity } = meal;

  const handleIncrease = () => {
    increaseMealQty(meal);
    addMealToCart(meal);
  };
  const handleDecrease = qty => {
    if (quantity === 0) return;
    decreaseMealQty(meal);
    removeMealFromCart(meal);
  };

  const handleExtraClick = (extra, state) => {
    if (!state) {
      addExtrasToMeal({ extra, parentId: meal.id });
    } else {
      removeExtrasFromMeal({ extra, parentId: meal.id });
    }
  };
  return (
    <div className="card meal-card">
      <div className="meal-card__image-wrapper">
        <div className="meal-card__image">
          <img src={image} alt={alt} />
        </div>
      </div>
      <div className="meal-card__content">
        <div className="align-justify meal-card__content-header">
          <div className="h6 font-bold meal-card__title">{name}</div>
          <div className="meal-card__quantity-control">
            <span onClick={() => handleDecrease(quantity)}>
              <Icon
                className="color-primary"
                type="minus-circle"
                theme="filled"
              />
            </span>
            <p>{quantity}</p>
            <span onClick={handleIncrease}>
              <Icon
                className="color-primary"
                type="plus-circle"
                theme="filled"
              />
            </span>
          </div>
        </div>
        {extras.length > 0 && (
          <div className="meal-card__content-body">
            <p className="h6">Select Extra</p>
            <div className="grid-x">
              {extras.map(extra => (
                <div
                  className={`cell shrink margin-vertical-1 margin-right-1 ${
                    !quantity ? "no-click" : ""
                  }`}
                  key={extra.id}
                >
                  <Pill extra={extra} handleClick={handleExtraClick}>
                    {extra.name}
                  </Pill>
                </div>
              ))}
            </div>
          </div>
        )}
        <div className="meal-card__content-footer">
          <p className="h6 font-bold">Price: </p>
          <p className="h6 font-bold">N {formatAmount(price)}</p>
        </div>
      </div>
    </div>
  );
};

MealCard.propTypes = {
  meal: PropTypes.object.isRequired,
  addMealToCart: PropTypes.func.isRequired,
  removeMealFromCart: PropTypes.func.isRequired,
};

export default MealCard;
