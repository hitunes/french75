import React from "react";
import { Button } from "antd";

const Hero = () => {
  return (
    <section className="hero grid-x align-middle">
      <div className="hero-bg">
        <img
          src="https://images.ctfassets.net/u5vvam7qql0f/6fAWkBB4TGckRzLVHXiXuM/133d496ac4bbc98187ebf635d5c7bfcd/elli-o-XoByiBymX20-unsplash.png"
          alt="Some meal"
        />
      </div>
      <div className="hero__content grid-container">
        <div className="grid-x align-center">
          <div className="cell">
            <p className="h6">Welcome to French75</p>
            <h2 className="h2 font-bold">
              Order food
              <br /> at affordable prices
            </h2>
            <br />
            <Button className="button">Place your order</Button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
