import React, { useEffect, useState } from "react";
import { contentfulClient } from "../../services/contentful-client";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";

const About = () => {
  const [aboutContent, setAboutContent] = useState("");

  useEffect(() => {
    const getAbout = async () => {
      const data = await contentfulClient.getEntries();
      const aboutDocument = data.items.filter(item => {
        return item.sys.contentType.sys.id === "aboutFrench75";
      });
      const content = aboutDocument[0].fields.content;
      setAboutContent(content);
    };
    getAbout();
  }, []);

  return (
    <div className="grid-x align-center bg-primary">
      <div className="cell medium-10 grid-container section--lg grid-x align-center">
        <div className="cell large-offset-7 large-5">
          <div>
            <h2 className="h3 font-bold text-uppercase">About French 75</h2>
            {documentToReactComponents(aboutContent)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
