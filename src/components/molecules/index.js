/**
 * * Single entrypoint to access all molecules.
 * * All molecules will be imported into here and exported as named modules.
 */

import MealCard from "./meal-card";
import Header from "./header";
import Hero from "./hero";
import About from "./about";

export { About, MealCard, Header, Hero };
