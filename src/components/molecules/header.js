import React, { useEffect, useState } from "react";
import assets from "../../assets/SiteAssets";
import classNames from "classnames";

const Header = () => {
  const [sticky, setSticky] = useState(false);

  useEffect(() => {
    if (window.pageYOffset > 400) {
      setSticky(true);
    } else {
      setSticky(false);
    }

    window.addEventListener("scroll", e => {
      window.pageYOffset > 400 ? setSticky(true) : setSticky(false);
    });
  }, []);

  const classes = classNames({
    header: true,
    "grid-x": true,
    "header--sticky": sticky
  });

  return (
    <header className={classes}>
      <div className="cell grid-container">
        <div className="grid-x align-justify align-middle">
          <div className="cell shrink header__brand-logo">
            <img src={assets.images.siteLogo} alt="French 75 Logo" />
          </div>
          <div className="cell shrink header__nav-container">
            <nav className="nav grid-y align-center align-middle">
              <ul>
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/">About</a>
                </li>
                <li>
                  <a href="/">Contact</a>
                </li>
                <li>
                  <a href="/">Order</a>
                </li>
              </ul>
            </nav>
          </div>
          <div className="hamburger-container">
            <div className="hamburger">
              <span className="bar"></span>
              <span className="bar"></span>
              <span className="bar"></span>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
