import React, { useState } from "react";
import { Header, Hero, About } from "./components/molecules/";
import { OrderForm, ShoppingCart } from "./components/organisms";
import { FloatingActionButton } from "./components/atoms";
import "./styles/app.scss";

import { MealCatalog } from "./components/organisms/";

function App() {
  const [cartOpen, setCartOpen] = useState(false);
  const [shoppingCartPlacement, setShoppingCartPlacement] = useState();

  const [orderFormVisible, setOrderFormVisible] = useState(false);

  const handleClick = () => {
    const placement = window.innerWidth > 800 ? "right" : "bottom";
    setShoppingCartPlacement(placement);
    setCartOpen(!cartOpen);
  };

  const handleOrderFormVisible = () => {
    const placement = window.innerWidth > 800 ? "right" : "bottom";
    setShoppingCartPlacement(placement);
    setCartOpen(false);
    setOrderFormVisible(!orderFormVisible);
  };


  return (
    <div className="grid-container-full">
      <Header></Header>
      <Hero />
      <div className="grid-container">
        <MealCatalog></MealCatalog>
      </div>
      <div className="cart">
        <ShoppingCart
          placement={shoppingCartPlacement}
          visible={cartOpen}
          updateVisible={handleClick}
          handleOrderFormVisible={handleOrderFormVisible}
        />

        <div className="shopping-cart__button">
          <FloatingActionButton
            icon="shopping-cart"
            handleClick={handleClick}
          />
        </div>
      </div>
      <OrderForm
        placement={shoppingCartPlacement}
        orderFormVisible={orderFormVisible}
        handleOrderFormVisible={handleOrderFormVisible}
      ></OrderForm>
      <About />
      <footer className="footer grid-x align-center">
        <p>Copyright © 2019 French75. All Rights Reserved.</p>
      </footer>
    </div>
  );
}

export default App;
