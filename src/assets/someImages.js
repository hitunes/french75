const images = {
  siteLogo: require("./img/logo-black.webp"),
  reactLogo: require("./some_images/react-logo.svg"),
  reduxLogo: require("./some_images/redux-logo.svg"),
  foundationLogo: require("./some_images/foundation-logo.svg"),
  framerMotionLogo: require("./some_images/framer-motion-logo.svg")
};

export default images;
